import Bot from "./util/bot.js";

export default class SimpleBot extends Bot{
    
    constructor(){
        super("SimpleBot")
    }
    
    simulate(dayInfo){
      
        // wenn der RSI14 unter 20 ist kauft der Bot mit dem gesamten verfügbaren Vermögen
        if (dayInfo.RSI14 < 20) {
            this.buy(this.getBalanceUSD, dayInfo);
        }

        // wenn der RSI14 über 80 ist verkauft der Bot alle Coins
        else if (dayInfo.RSI14 > 80) {
            this.sell(this.getBalanceCOIN, dayInfo);
        }
    }
}