import Bot from "./bot.js";

/**
 * This bot simply buys ALL at the first day and sells ALL at the last. It therefore gives an indication how well other bots perform
 */
export default class MarketBot extends Bot{
    
    constructor(){
        super("MarketBot")
    }
    
    run(values, startDate, endDate){
        var startIndex = this.indexFromDate(values,startDate);
        var endIndex = this.indexFromDate(values, endDate);
        if(startIndex < 0 || endIndex < 0){
            console.error("Der angegebene Zeitraum ist nicht valide");
            return;
        }
        this.buy(this.getBalanceUSD, values[startIndex]);
        this.sell(this.getBalanceCOIN, values[endIndex]);
    }
    
}