/**
 * Superclass for creating bots
 */
const INITIAL_USD = 100;
const FEE = 0//.0005;

export default class Bot {

    name = "Unnamed Bot";
    initialUSD = 0;
    accountUSD = 0;
    accountCOIN = 0;
    transactionFee = 0;

    /**
     * Defines a new Bot with a given amount of USD
     * @param {string} name the name of the bot
     * @param {number} initialUSD the start amount of money
     * @param {number} transactionFee the transaction fee that the exchange takes (as a fraction)
     */
    constructor(name) {
        this.name = name ?? "Unnamed bot";
        this.transactionFee = FEE;     //transactionFee ?? 0;
        this.initialUSD = INITIAL_USD  //initialUSD ?? 0;
        this.reset();
    }

    reset(){
        this.accountUSD = this.initialUSD;
        this.accountCOIN = 0;
    }

    get getName(){
        return this.name;
    }

    get getBalanceUSD(){
        return this.accountUSD;
    }

    get getBalanceCOIN(){
        return this.accountCOIN;
    }

    /**
     * Sells a specified amount of the coin to the specified price
     * @param {number} coinAmount the amount of coin that you want to sell
     * @param {Object} dayInfo information about the current day of trading
     */
    sell(coinAmount, dayInfo) {
        var result = (coinAmount * dayInfo.close);
        this.accountCOIN -= coinAmount;
        this.accountUSD += result - (this.transactionFee * Math.abs(result));

        console.log(("[" + this.getName + "]").padEnd(14, " ") +  "SELL  " 
            + "Date: " + dayInfo.date.padEnd(17, " ")  
            + "Amount: " + (coinAmount.toFixed(2) + " COIN").padStart(7," ").padEnd(17, " ") 
            + "Price: " + (dayInfo.close.toFixed(2) + " USD").padStart(7," ").padEnd(17, " ") );
    }

    /**
     * buy a specified amount of a coin
     * @param {number} usdAmount the amount of USD for which you want to buy coin
     * @param {Object} dayInfo information about the current day of trading
     */
    buy(usdAmount, dayInfo) {
        var result = (usdAmount / dayInfo.close);
        this.accountUSD -= usdAmount;
        this.accountCOIN += result - (this.transactionFee * Math.abs(result));

        console.log(("[" + this.getName + "]").padEnd(14, " ") +  "BUY   " +
             "Date: " + dayInfo.date.padEnd(17, " ")  + 
             "Amount: " + (usdAmount.toFixed(2) + " USD ").padStart(7," ").padEnd(17, " ")  + 
             "Price: " + (dayInfo.close.toFixed(2) + " USD").padStart(7," ").padEnd(17, " ") );
    }

    /**
     * run the bot on a set dataset over a given period of time
     * @param {Array} values the dataset over which to run 
     * @param {string} startDate the first date for trading (in DD.MM.YYYY Format)
     * @param {string} endDate the last date for trading (in DD.MM.YYYY Format)
     */
    run(values, startDate, endDate){
        var startIndex = this.indexFromDate(startDate);
        var endIndex = this.indexFromDate(endDate);
        if(startIndex < 0 || endIndex < 0){
            console.error("Der angegebene Zeitraum ist nicht valide");
            return;
        }
        for(var i = startIndex; i <= Math.min(endIndex, values.length); i++){
            this.simulate(values[i]);
        }
    }

    /**
     * give the the bot an array of day data up to the day you want to simulate
     * @param {Object} startDate the trading information of that day
     */
    simulate(dayData){
        console.error("Bot Simulate-ERROR: No simulation actions are defined. Override the simulate(..) function")
    }

    /**
     * compare the performance of a bot to another over a given period of time
     * @param {Bot[]} bots the second bot to compare the results to
     * @param {Array} values the array of trading data
     * @param {string} startDate the first date for trading (in DD.MM.YYYY Format)
     * @param {string} endDate the last date for trading (in DD.MM.YYYY Format)
     */
    compareTo(bots, values, startDate, endDate) {
        console.log("\n");
        console.log("BotCompare:   == PERFORMANCE COMPARISON ==");

        console.log("BotCompare:\nBotCompare:   running bot '" + this.getName + "' (this bot):");
            this.reset();
            this.run(values, startDate, endDate);

        for(var bot of bots){
            console.log("BotCompare:\nBotCompare:   running bot '" + bot.getName + "':");
            bot.reset();
            bot.run(values, startDate, endDate);
        }
        
        console.log("BotCompare:\nBotCompare:   ================================ RESULTS ================================");
        console.log("BotCompare:   " + "BotName".padEnd(20, " ") + "Profit".padEnd(17, " ")  + "Performance compared to this Bot");
        console.log("BotCompare:   " + "-------".padEnd(20, " ") + "------".padEnd(17, " ")  + "--------------------------------");
        console.log("BotCompare:   " + ("'" + this.getName + "'").padEnd(20, " ") + ((this.getBalanceUSD-INITIAL_USD).toFixed(2) + " USD").padStart(15," ")  + "  " + "- n.a. -");

        for(var bot of bots){
            console.log("BotCompare:   " + ("'" + bot.getName + "'").padEnd(20, " ") + ((bot.getBalanceUSD - INITIAL_USD).toFixed(2) + " USD").padStart(15," ") + "  " + (100/this.getBalanceUSD*bot.getBalanceUSD - 100).toFixed(2) + "%");
        }
        console.log("\n");
    }

    indexFromDate(values, dateString) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].date === dateString) {
                return i;
            }
        }
        return -1;
    }
}
