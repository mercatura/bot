import SimpleBot from "./bots/simple_bot.js";
import MarketBot from "./bots/util/market_bot.js";
import values from "./data/BTCUSD1520.js";

// Marktvergleichs-Bot. Kauft am Anfang, verkauft am Ende
var marketBot = new MarketBot();

var simpleBot = new SimpleBot();

// Bots vergleichen. Neue Bots hinter simpleBot in das Array einfügen
marketBot.compareTo([simpleBot],values, "01.01.2016", "01.10.2020");